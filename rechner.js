function dezbin (dez)
{
var potencies = new Array();
var binary = '';
for (var i = 0; i > -1; i++)
{
var potency = Math.pow(2, i);
if (potency > dez) { break; }
potencies[i] = potency;
}

potencies.reverse();

for (var j = 0; j < potencies.length; j++)
{
var position = potencies[j];
var zeroOne = parseInt(dez / position);
binary += zeroOne + '';
dez -= potencies[j] * zeroOne;
}

document.dezbinrechner.bin.value = binary;
}

function bindez (bin)
{
var bin = bin + '';
var digits = bin.split('');
digits.reverse();
var dez = 0;
for (var i = 0; i < digits.length; i++)
{
var result = digits[i] * Math.pow(2, i);
dez += result;
}

document.dezbinrechner.dez.value = dez;
}